

function getSpeakingStats(){
            if (!game.user.isGM){ return };  

        totalTimeSpeaked = game.users.entities.filter(u => u.data.speakerstats).reduce(function(a,u){
                                return a + u.data.speakerstats.timeSpeaking;
                            }, 0);
        
        let message = game.users.entities.filter(u => u.data.speakerstats).reduce(function(a, u){
                                return a + "<p><strong>" + u.name + ": </strong>" + (u.data.speakerstats.timeSpeaking/1000/60).toFixed(2) + "Minutes | " + u.data.speakerstats.timeSpeaking/totalTimeSpeaked*100 + "%</p>";
                            }, '<p><strong>Total: </strong>' + (totalTimeSpeaked/1000/60).toFixed(2) + "Minutes.</p><hr>");
        
        
        ChatMessage.create({
            user: game.user._id,
            content: "<p>User time talking</p>" + message,
            whisper: game.users.entities.filter(u => u.isGM).map(u => u._id),
            type: CONST.CHAT_MESSAGE_TYPES.WHISPER
        });
}        


Hooks.on("setup", function() {

    CameraViews.prototype.setUserIsSpeaking = function(userId, speaking) {
    let box = this._getCameraBox(userId)[0];
    if ( box ) box.classList.toggle("speaking", speaking);
    
         
    if (game.user.isGM){        
        userTalking = game.users.entities.find(u => u._id == userId);
        if (userTalking.data.speakerstats == null) {
            userTalking.data.speakerstats = { startSpeaking: false, timeSpeaking: false }
            
        };
            
        if(userTalking.data.speakerstats.startSpeaking == false && speaking)
        {
            userTalking.data.speakerstats.startSpeaking = Date.now();            
        }
        if(userTalking.data.speakerstats.startSpeaking != false && !speaking)
        {
            lastTimeSpeaking = Date.now() - userTalking.data.speakerstats.startSpeaking;
            userTalking.data.speakerstats.timeSpeaking = userTalking.data.speakerstats.timeSpeaking + lastTimeSpeaking;
            userTalking.data.speakerstats.startSpeaking = false;
        }        
    }
    
  }
    
});

Hooks.on('renderSceneControls', (controls, html) => {
    if (game.user.isGM){   
        const speakstatsBtn = $(
            `<li class="scene-control">
                <i class="fas fa-comments"></i>
            </li>`
        );
        html.append(speakstatsBtn);
        speakstatsBtn[0].addEventListener('click', evt => {
            evt.stopPropagation();
            this.getSpeakingStats();
        });
    }
});

