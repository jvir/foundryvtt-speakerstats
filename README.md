# SpeakerStats
This FoundryVTT Module will add an button to the GM toolbar that allows to GM visualize the time spoken (in audio) by each user and and display them as chat message.

## Setup
To install this module, go to the World Configuration and Setup, Addon Modules, Install Module.
Then you may copy this url https://gitlab.com/mesfoliesludiques/foundryvtt-modbox/-/raw/master/module.json.

## Limitations
This module only saves the statistics in memory in the GM session, when the page is refreshed the statistics are restarted.

## Contributions
Every contribution is welcome.
